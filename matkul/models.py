from django.db import models

class MataKuliah(models.Model):
    namaMatkul = models.CharField(max_length=25)
    dosen = models.CharField(max_length=50)
    SKS = models.IntegerField()
    deskripsi = models.TextField()
    semester = models.CharField(max_length=15)
    ruang = models.CharField(max_length=20)