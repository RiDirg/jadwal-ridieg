from django.urls import path
from . import views

urlpatterns = [
    path('', views.formMk),
    path('tabel/', views.tabelMK),
    path('matkul/<str:matkul_pk>/', views.matkul, name='lihat_matkul'),
    path('deletemk/<str:deletdis>/', views.delet)    
]
